from views import MainView, PingView


def setup_routes(app):
    app.router.add_get('/api/ping', PingView)
    app.router.add_post('/api/', MainView)
    app.router.add_get('/api/{pk}', MainView)
$(function() {
    $( document ).ready(function() {

        $('#utbdlSubmit').click(function(){
            var urlObj = $("#inputUrl");
            var url = urlObj.val();
            var artist = $("#inputArtist").val();
            var song = $("#inputSong").val();
            var isValidUrl = validateYouTubeUrl(urlObj);
            if (!isValidUrl) {
                return false;
            }

            var apiUrl = "/api/";
            var data = { url: url, song: song, artist: artist };

            //$.post(apiUrl, data)
            //    .done(function( data ) {
            //        console.log( "Data Loaded: " + data );
            //    })
            //    .fail(function(err) {
            //        console.log( "error" + err);
            //    });

            $.ajax ({
                url: apiUrl,
                type: "POST",
                data: data,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function(){
                    console.log( "Data Loaded: " + data );
                }
            });
        });



    });
});

function validateYouTubeUrl(urlObj) {
    var url = urlObj.val();
    if (url != undefined || url != '') {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length == 11) {
            // Do anything for being valid
            // if need to change the url to embed url then use below line
            $('#videoObject').attr('src', 'https://www.youtube.com/embed/' + match[2] + '?autoplay=1&enablejsapi=1');
            return true;
        } else {
            $(urlObj).closest('.form-group').addClass('has-error');
            return false;
        }
    }
}
import redis
import logging

from aiohttp import web
from aiohttp.web import View
from aiohttp.errors import BadHttpMessage
from taskprocess import process
from celery.result import AsyncResult

from config import redis_backend

log = logging.getLogger(__name__)


class MainView(View):

    async def post(self):
        """
        Creates a new request to process an url
        :return:
        """
        try:
            data = await self.request.json()
        except Exception as ex:
            log.error("Exception receiving data: {}".format(ex))
            raise BadHttpMessage(message="Could not parse received data")

        result = process.delay(data)

        response = {
            'id': result.id,
            'ready': result.ready(),
            'state': result.state if result.ready() else None
        }

        return web.json_response(data=response)

    async def get(self):
        """
        Get info about a task
        :return:
        """
        pk = self.request.match_info['pk']
        res = AsyncResult(pk, app=process)

        data = {
            'id': pk,
            'state': res.state,
            'ready': res.ready(),
        }

        r = redis.StrictRedis(host=redis_backend['hostname'],
                              port=redis_backend['port'],
                              db=redis_backend['vhost'])

        if res.ready():
            name = 'f:{}'.format(pk)

            info = r.hmget(name=name, keys=('filename', 'song', 'artist'))
            data['filename'] = info[0].decode('utf-8')
            data['song'] = info[1].decode('utf-8')
            data['artist'] = info[2].decode('utf-8')

        return web.json_response(data=data)


class PingView(View):
    async def get(self):
        return web.json_response(data={'status': 'OK'})


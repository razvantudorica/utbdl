import logging.config
import argparse

import asyncio
from aiohttp import web

from config import access_log

from routes import setup_routes

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process server arguments')
    parser.add_argument('--port', default=8080, type=int, help='The port')
    parser.add_argument('--host', default='127.0.0.1', help='The host/ip')
    args = parser.parse_args()

    logging.basicConfig(filename=access_log, level=logging.DEBUG)

    logger = logging.getLogger("api")

    loop = asyncio.get_event_loop()
    app = web.Application(loop=loop, logger=logger)

    setup_routes(app)

    web.run_app(app, host=args.host, port=args.port)

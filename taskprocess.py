import os
import redis
import ntpath

from celery import Celery
from celery.utils.log import get_task_logger
from config import use_amqp, amqp_broker, amqp_backend, \
    redis_broker, redis_backend, downloader, download_dir, ffmpeg, id3v2

from subprocess import Popen, PIPE

if use_amqp:
    broker_url = '{protocol}://{user}:{password}@{hostname}/{vhost}'.format(**amqp_broker)
    backend_url = '{protocol}://{user}:{password}@{hostname}/{vhost}'.format(**amqp_backend)
else:
    broker_url = '{protocol}://{hostname}:{port}/{vhost}'.format(**redis_broker)
    backend_url = '{protocol}://{hostname}:{port}/{vhost}'.format(**redis_backend)

app = Celery('tasks', backend=backend_url, broker=broker_url)

logger = get_task_logger(__name__)


@app.task(bind=True)
def process(self, data):
    """
    Data should be a dict with the keys:
    url
    artist
    song
    album (optional)
    year (optional)
    :param data:
    :return:
    """

    url = data.get('url')
    artist = data.get('artist').strip()
    song = data.get('song').strip()
    album = data.get('album', '')
    year = data.get('year', '')

    filename = '{}/{} - {}'.format(download_dir, artist, song)

    # cmd = [
    #     downloader,
    #     '-x',
    #     '--audio-format=mp3',
    #     url,
    #     '-o',
    #     filename
    # ]
    cmd = [
        downloader,
        url,
        '-o',
        filename
    ]

    logger.info("Execute: {}".format(' '.join(cmd)))
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate()
    ret = proc.wait()
    if ret != 0:
        logger.error("Error: {}"
                     "ret={};"
                     "stdout={};"
                     "stderr={}".format(cmd, ret, stdout, stderr))
        return

    mkv_file = filename + '.mkv'
    mp3file = filename + '.mp3'
    cmd = [
        ffmpeg,
        '-i',
        mkv_file,
        mp3file
    ]
    logger.info("Execute: {}".format(' '.join(cmd)))
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate()
    ret = proc.wait()

    if ret != 0:
        logger.error("Error: {}"
                     "ret={};"
                     "stdout={};"
                     "stderr={}".format(cmd, ret, stdout, stderr))
        return

    os.remove(mkv_file)

    cmd = [
        id3v2,
        '-t {}'.format(song),
        '-a {}'.format(artist),
        '-y {}'.format(year),
        '-A {}'.format(album),
        mp3file
    ]

    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
    proc.communicate()
    proc.wait()

    r = redis.StrictRedis(host=redis_backend['hostname'],
                          port=redis_backend['port'],
                          db=redis_backend['vhost'])

    store_data = {'filename': ntpath.basename(mp3file), 'artist': artist, 'song': song}
    key = 'f:{}'.format(self.request.id)

    r.hmset(key, store_data)


# start this like: celery -A taskprocess worker --loglevel=info
# in your app
# from taskprogress import process
# result = process.delay(4, 7)
# result.get(timeout=1)

# README #

Youtube Downloader API

### What is this project for? ###

Accept youtube url and other parameters (as author, title, output format) and using python youtube-downloader script download and add metadata to the file.

### Install ###

```
git clone git@bitbucket.org:razvantudorica/utbdl.git utbdl

apt-get install python3 python3-venv
mkdir env
python3 -m venv env
source env/bin/activate
cd utbdl
pip install -r requirements.txt
```

### Deploy ###

First time:

* Install the above packages
* deactivate the virtual env
* Install nginx and use the sys/nginx.config as configuration file
* Install supervisord

```
apt-get install supervisor

# copy the config file from sys folder
cp sys/utbdl.conf /etc/supervisor/conf.d/utbdl.conf

# start supervisord
service supervisor start
# or, if it is started
supervisorctl update
```


### Usage ###

* Fill the config.py and create the necessary folders

* Start celery daemon:

```bash
celery -A taskprocess worker --loglevel=info
```

* Start the http server

```bash
python main.py
```

* Do an http call

```bash
http POST http://localhost:8080/ url=https://www.youtube.com/watch?v=pL4uESRCnv8 artist=Aerosmith song="Walk This Way"
```
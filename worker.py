import pika
import json
import subprocess


def callback(ch, method, properties, message):
    data = json.loads(message)

    url = data['url']
    artist = data.get('artist', 'unknown')
    title = data.get('title', 'unknown')

    filename = '{} - {}.mp3'.format(artist, title)
    cmd = ['youtube-dl',
           url,
           '-x', '--audio-format=mp3',
           '-o', filename]

    try:
        print("Execute: {}".format(' '.join(cmd)))
        res = subprocess.check_output(cmd)
    except:
        print("Failed to execute: {}".format(' '.join(cmd)))

    print(" [x] Done")
    ch.basic_ack(delivery_tag = method.delivery_tag)


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='task_queue', durable=True)
    print(' [*] Waiting for messages. To exit press CTRL+C')

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(callback,
                          queue='task_queue')

    channel.start_consuming()
